# NeverTheLess - Helm Repository

##### Table of contents

- [Get Started](#get-started)
  - [Initialization](#initialization)
  - [Delete](#delete)
- [Charts](#charts)
  - [azuredevops-agent](#azuredevops-agent)
  - [azuredevops-deploymentgroup](#azuredevops-deploymentgroup)

## Get Started

**Helm Chart Repository**: https://helm.nevertheless.space.

### Initialization
To use your new Chart repository, run on your local computer:
```bash
helm repo add nevertheless.space https://helm.nevertheless.space
helm repo update

helm install --namespace=<namespace-name> --create-namespace <release-name> <chart-name>
```

### Delete

To uninstall the Helm application:

```bash
helm uninstall <release-name> --namespace <namespace>
kubectl delete namespace <namespace>
```

## Charts

### azuredevops-agent

- Current version documentation: [README.md](https://gitlab.com/nevertheless.space/helm-charts/-/blob/master/charts/azuredevops-agent/latest/README.md).
- Description: Azure DevOps Agent for Kubernetes.

### azuredevops-deploymentgroup

- Current version documentation: [README.md](https://gitlab.com/nevertheless.space/helm-charts/-/blob/master/charts/azuredevops-deploymentgroup/latest/README.md).
- Description: Azure DevOps Deployment Group Agent for Kubernetes.
