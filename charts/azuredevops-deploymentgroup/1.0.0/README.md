### azuredevops-deploymentgroup

- Name: nevertheless.space/azuredevops-deploymentgroup
- Version: 1.0.0
- Variables:
    - **azureDevOps.url**: Azure DevOps - URL
    - **azureDevOps.pat**: Azure DevOps - Personal Access Token (PAT)
    - **azuredevops.project**: Azure DevOps - Project Name
    - **azuredevops.collection**: Azure DevOps - Collection Name
    - **azuredevops.deploymentGroup**: Azure DevOps - Deployment Group Name
    - **azuredevops.tags**: Azure DevOps - Deployments Group Tags
        - To escape "," in Helm: ```"k8s\, linux"```

#### Code Example

##### Linux
```bash
chart_name="nevertheless.space/azuredevops-deploymentgroup"
chart_version="1.0.0"

release_name="azuredevops-deploymentgroup"
namespace_name="azuredevops-deploymentgroup"

azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)
azuredevops_project="Project 01"            # Azure DevOps - Project Name
azuredevops_collection="My Collection"      # Azure DevOps - Collection Name
azuredevops_deploymentGroup="K8s Agents"    # Azure DevOps - Deployment Group Name
azuredevops_tags="k8s\, linux"              # Azure DevOps - Deployments Group Tags

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name \
--set azureDevOps.url="$azuredevops_url" \
--set azureDevOps.pat="$azuredevops_pat" \
--set azureDevOps.project="$azuredevops_project" \
--set azureDevOps.collection="$azuredevops_collection" \
--set azureDevOps.deploymentGroup="$azuredevops_deploymentGroup" \
--set azureDevOps.tags="$azuredevops_tags" \
--version="$chart_version"
```

##### Powershell
```powershell
$chart_name="nevertheless.space/azuredevops-deploymentgroup"
$chart_version="1.0.0"

$release_name="azuredevops-deploymentgroup"
$namespace_name="azuredevops-deploymentgroup"

$azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
$azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)
$azuredevops_project="Project 01"            # Azure DevOps - Project Name
$azuredevops_collection="My Collection"      # Azure DevOps - Collection Name
$azuredevops_deploymentGroup="K8s Agents"    # Azure DevOps - Deployment Group Name
$azuredevops_tags="k8s\, linux"              # Azure DevOps - Deployments Group Tags - "\" TO ESCAPE ","

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name `
--set azureDevOps.url="$azuredevops_url" `
--set azureDevOps.pat="$azuredevops_pat" `
--set azureDevOps.project="$azuredevops_project" `
--set azureDevOps.collection="$azuredevops_collection" `
--set azureDevOps.deploymentGroup="$azuredevops_deploymentGroup" `
--set azureDevOps.tags="$azuredevops_tags" `
--version="$chart_version"
```