### azuredevops-deploymentgroup

#### Overview

- Name: nevertheless.space/azuredevops-deploymentgroup
- Version: 1.1.1
- Variables:
    - **azureDevOps.url**: Azure DevOps - URL
    - **azureDevOps.pat**: Azure DevOps - Personal Access Token (PAT)
    - **azuredevops.project**: Azure DevOps - Project Name
    - **azuredevops.collection**: Azure DevOps - Collection Name
    - **azuredevops.deploymentGroup**: Azure DevOps - Deployment Group Name
    - **azuredevops.tags**: Azure DevOps - Deployments Group Tags
        - To escape "," in Helm: ```"k8s\, linux"```
    - **workingDir**: Azure DevOps - Agent working directory (Default: "_work")
    - **replicaCount**: Default: 1
    - **statefulSet.enabled**: Default: false
    - **statefulSet.storageClassName**: Default: "default"
    - **statefulSet.storageSize**: Default: "20Gi"
    - **limits.enabled**: Default: false
    - **limits.requestedMemory**: Default: "128Mi"
    - **limits.requestedCPU**: Default: "250m"
    - **limits.memoryLimit**: Default: "1Gi"
    - **limits.CPULimit**: Default: "1"

#### Deployment - Code Example

##### Values.yaml
```bash
helm install -f values.yaml --namespace="<namespace_name>" --create-namespace "<my_release_name>" nevertheless.space/azuredevops-deploymentgroup:1.1.1 
```
```yaml
replicaCount: 1

azureDevOps:
  url: "https://myazuredevops.com/tfs"
  project: "Project 1"
  collection: "Collection 2"
  deploymentGroup: "Kubernetes-Agents"
  pat: "XXXXXXXXXXXXXXXXXXXXX"
  tags: "k8s, linux"

workingDir: "_work"

statefulSet:
  enabled: false
  storageClassName: "default"
  storageSize: "20Gi"

limits:
  enabled: false
  requestedMemory: "128Mi"
  requestedCPU: "250m"
  memoryLimit: "1Gi"
  CPULimit: "1"
```

##### Linux
```bash
chart_name="nevertheless.space/azuredevops-deploymentgroup"
chart_version=" 1.1.1"

release_name="azuredevops-deploymentgroup"
namespace_name="azuredevops-deploymentgroup"

azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)
azuredevops_project="Project 01"            # Azure DevOps - Project Name
azuredevops_collection="My Collection"      # Azure DevOps - Collection Name
azuredevops_deploymentGroup="K8s Agents"    # Azure DevOps - Deployment Group Name
azuredevops_tags="k8s\, linux"              # Azure DevOps - Deployments Group Tags

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name \
--set azureDevOps.url="$azuredevops_url" \
--set azureDevOps.pat="$azuredevops_pat" \
--set azureDevOps.project="$azuredevops_project" \
--set azureDevOps.collection="$azuredevops_collection" \
--set azureDevOps.deploymentGroup="$azuredevops_deploymentGroup" \
--set azureDevOps.tags="$azuredevops_tags" \
--version="$chart_version"
```

##### Powershell
```powershell
$chart_name="nevertheless.space/azuredevops-deploymentgroup"
$chart_version=" 1.1.1"

$release_name="azuredevops-deploymentgroup"
$namespace_name="azuredevops-deploymentgroup"

$azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
$azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)
$azuredevops_project="Project 01"            # Azure DevOps - Project Name
$azuredevops_collection="My Collection"      # Azure DevOps - Collection Name
$azuredevops_deploymentGroup="K8s Agents"    # Azure DevOps - Deployment Group Name
$azuredevops_tags="k8s\, linux"              # Azure DevOps - Deployments Group Tags - "\" TO ESCAPE ","

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name `
--set azureDevOps.url="$azuredevops_url" `
--set azureDevOps.pat="$azuredevops_pat" `
--set azureDevOps.project="$azuredevops_project" `
--set azureDevOps.collection="$azuredevops_collection" `
--set azureDevOps.deploymentGroup="$azuredevops_deploymentGroup" `
--set azureDevOps.tags="$azuredevops_tags" `
--version="$chart_version"
```

#### StatefulSet - Code Example

##### Values.yaml
```bash
helm install -f values.yaml --namespace="<namespace_name>" --create-namespace "<my_release_name>" nevertheless.space/azuredevops-deploymentgroup:1.1.1 
```
```yaml
replicaCount: 1

azureDevOps:
  url: "https://myazuredevops.com/tfs"
  project: "Project 1"
  collection: "Collection 2"
  deploymentGroup: "Kubernetes-Agents"
  pat: "XXXXXXXXXXXXXXXXXXXXX"
  tags: "k8s, linux"

workingDir: "_work"

statefulSet:
  enabled: true
  storageClassName: "default"
  storageSize: "20Gi"

limits:
  enabled: false
  requestedMemory: "128Mi"
  requestedCPU: "250m"
  memoryLimit: "1Gi"
  CPULimit: "1"
```

##### Linux
```bash
chart_name="nevertheless.space/azuredevops-deploymentgroup"
chart_version=" 1.1.1"

release_name="azuredevops-deploymentgroup"
namespace_name="azuredevops-deploymentgroup"

azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)
azuredevops_project="Project 01"            # Azure DevOps - Project Name
azuredevops_collection="My Collection"      # Azure DevOps - Collection Name
azuredevops_deploymentGroup="K8s Agents"    # Azure DevOps - Deployment Group Name
azuredevops_tags="k8s\, linux"              # Azure DevOps - Deployments Group Tags

replicaCount="2"
statefulSet_enabled="true"
statefulSet_storageClassName="default"
statefulSet_storageSize="20Gi"

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name \
--set azureDevOps.url="$azuredevops_url" \
--set azureDevOps.pat="$azuredevops_pat" \
--set azureDevOps.project="$azuredevops_project" \
--set azureDevOps.collection="$azuredevops_collection" \
--set azureDevOps.deploymentGroup="$azuredevops_deploymentGroup" \
--set azureDevOps.tags="$azuredevops_tags" \
--set statefulSet.enabled=$statefulSet_enabled \
--set statefulSet.storageClassName=$statefulSet_storageClassName \
--set statefulSet.storageSize=$statefulSet_storageSize \
--set replicaCount=$replicaCount \
--version="$chart_version"
```

##### Powershell
```powershell
$chart_name="nevertheless.space/azuredevops-deploymentgroup"
$chart_version=" 1.1.1"

$release_name="azuredevops-deploymentgroup"
$namespace_name="azuredevops-deploymentgroup"

$azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
$azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)
$azuredevops_project="Project 01"            # Azure DevOps - Project Name
$azuredevops_collection="My Collection"      # Azure DevOps - Collection Name
$azuredevops_deploymentGroup="K8s Agents"    # Azure DevOps - Deployment Group Name
$azuredevops_tags="k8s\, linux"              # Azure DevOps - Deployments Group Tags - "\" TO ESCAPE ","

$replicaCount="2"
$statefulSet_enabled="true"
$statefulSet_storageClassName="default"
$statefulSet_storageSize="20Gi"

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name `
--set azureDevOps.url="$azuredevops_url" `
--set azureDevOps.pat="$azuredevops_pat" `
--set azureDevOps.project="$azuredevops_project" `
--set azureDevOps.collection="$azuredevops_collection" `
--set azureDevOps.deploymentGroup="$azuredevops_deploymentGroup" `
--set azureDevOps.tags="$azuredevops_tags" `
--set statefulSet.enabled=$statefulSet_enabled `
--set statefulSet.storageClassName=$statefulSet_storageClassName `
--set statefulSet.storageSize=$statefulSet_storageSize `
--set replicaCount=$replicaCount `
--version="$chart_version"
```