### azuredevops-agent

#### Overview

- Name: nevertheless.space/azuredevops-agent
- Version: 1.1.0
- Variables:
    - **azureDevOps.url**: Azure DevOps - URL
    - **azureDevOps.pool**: Azure DevOps - Agents Pool Name
    - **azureDevOps.pat**: Azure DevOps - Personal Access Token (PAT)
    - **workingDir**: Azure DevOps - Agent working directory (Default: "_work")
    - **replicaCount**: Default: 1
    - **statefulSet.enabled**: Default: false
    - **statefulSet.storageClassName**: Default: "default"
    - **statefulSet.storageSize**: Default: "20Gi"
    - **limits.enabled**: Default: false
    - **limits.requestedMemory**: Default: "128Mi"
    - **limits.requestedCPU**: Default: "250m"
    - **limits.memoryLimit**: Default: "1Gi"
    - **limits.CPULimit**: Default: "1"

#### Deployment - Code Example

##### Values.yaml
```bash
helm install -f values.yaml --namespace="<namespace_name>" --create-namespace "<my_release_name>" nevertheless.space/azuredevops-agent:1.1.0 
```
```yaml
replicaCount: 1

azureDevOps:
  url: "https://myazuredevops.com/tfs"
  pool: "Agent-Pool-1"
  pat: "XXXXXXXXXXXXXXXXXXXXX"

workingDir: "_work"

statefulSet:
  enabled: false
  storageClassName: "default"
  storageSize: "20Gi"

limits:
  enabled: false
  requestedMemory: "128Mi"
  requestedCPU: "250m"
  memoryLimit: "1Gi"
  CPULimit: "1"
```

##### Linux
```bash
chart_name="nevertheless.space/azuredevops-agent"
chart_version="1.1.0"

release_name="azuredevops-agent"
namespace_name="azuredevops-agent"

azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
azuredevops_pool="Agents-Pool-1"            # Azure DevOps - Agents Pool Name
azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name \
--set azureDevOps.url="$azuredevops_url" \
--set azureDevOps.pool="$azuredevops_pool" \
--set azureDevOps.pat="$azuredevops_pat" \
--version="$chart_version"
```

##### Powershell
```powershell
$chart_name="nevertheless.space/azuredevops-agent"
$chart_version="1.1.0"

$release_name="azuredevops-agent"
$namespace_name="azuredevops-agent"

$azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
$azuredevops_pool="Agents-Pool-1"            # Azure DevOps - Agents Pool Name
$azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name `
--set azureDevOps.url="$azuredevops_url" `
--set azureDevOps.pool="$azuredevops_pool" `
--set azureDevOps.pat="$azuredevops_pat" `
--version="$chart_version"
```

#### StatefulSet - Code Example

##### Values.yaml
```bash
helm install -f values.yaml --namespace="<namespace_name>" --create-namespace "<my_release_name>" nevertheless.space/azuredevops-agent:1.1.0 
```
```yaml
replicaCount: 1

azureDevOps:
  url: "https://myazuredevops.com/tfs"
  pool: "Agent-Pool-1"
  pat: "XXXXXXXXXXXXXXXXXXXXX"

workingDir: "_work"

statefulSet:
  enabled: true
  storageClassName: "default"
  storageSize: "20Gi"

limits:
  enabled: false
  requestedMemory: "128Mi"
  requestedCPU: "250m"
  memoryLimit: "1Gi"
  CPULimit: "1"
```

##### Linux
```bash
chart_name="nevertheless.space/azuredevops-agent"
chart_version="1.1.0"

release_name="azuredevops-agent"
namespace_name="azuredevops-agent"

azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
azuredevops_pool="Agents-Pool-1"            # Azure DevOps - Agents Pool Name
azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)

replicaCount="2"
statefulSet_enabled="true"
statefulSet_storageClassName="default"
statefulSet_storageSize="20Gi"

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name \
--set azureDevOps.url="$azuredevops_url" \
--set azureDevOps.pool="$azuredevops_pool" \
--set azureDevOps.pat="$azuredevops_pat" \
--set statefulSet.enabled=$statefulSet_enabled \
--set statefulSet.storageClassName=$statefulSet_storageClassName \
--set statefulSet.storageSize=$statefulSet_storageSize \
--set replicaCount=$replicaCount \
--version="$chart_version"
```

##### Powershell
```powershell
$chart_name="nevertheless.space/azuredevops-agent"
$chart_version="1.1.0"

$release_name="azuredevops-agent"
$namespace_name="azuredevops-agent"

$azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
$azuredevops_pool="Agents-Pool-1"            # Azure DevOps - Agents Pool Name
$azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)

$replicaCount="2"
$statefulSet_enabled="true"
$statefulSet_storageClassName="default"
$statefulSet_storageSize="20Gi"

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name `
--set azureDevOps.url="$azuredevops_url" `
--set azureDevOps.pool="$azuredevops_pool" `
--set azureDevOps.pat="$azuredevops_pat" `
--set statefulSet.enabled=$statefulSet_enabled `
--set statefulSet.storageClassName=$statefulSet_storageClassName `
--set statefulSet.storageSize=$statefulSet_storageSize `
--set replicaCount=$replicaCount `
--version="$chart_version"
```