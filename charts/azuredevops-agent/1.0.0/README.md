### azuredevops-agent
- Name: nevertheless.space/azuredevops-agent
- Version: 1.0.0
- Variables:
    - **azureDevOps.url**: Azure DevOps - URL
    - **azureDevOps.pool**: Azure DevOps - Agents Pool Name
    - **azureDevOps.pat**: Azure DevOps - Personal Access Token (PAT)

#### Code Example

##### Linux
```bash
chart_name="nevertheless.space/azuredevops-agent"
chart_version="1.0.0"

release_name="azuredevops-agent"
namespace_name="azuredevops-agent"

azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
azuredevops_pool="Agents-Pool-1"            # Azure DevOps - Agents Pool Name
azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name \
--set azureDevOps.url="$azuredevops_url" \
--set azureDevOps.pool="$azuredevops_pool" \
--set azureDevOps.pat="$azuredevops_pat" \
--version="$chart_version"
```

##### Powershell
```powershell
$chart_name="nevertheless.space/azuredevops-agent"
$chart_version="1.0.0"

$release_name="azuredevops-agent"
$namespace_name="azuredevops-agent"

$azuredevops_url="https://azuredevops/tfs"   # Azure DevOps - URL
$azuredevops_pool="Agents-Pool-1"            # Azure DevOps - Agents Pool Name
$azuredevops_pat="XXXXXXXXXXXXXXXXXX"        # Azure DevOps - Personal Access Token (PAT)

helm install --namespace="$namespace_name" --create-namespace $release_name $chart_name `
--set azureDevOps.url="$azuredevops_url" `
--set azureDevOps.pool="$azuredevops_pool" `
--set azureDevOps.pat="$azuredevops_pat" `
--version="$chart_version"
```